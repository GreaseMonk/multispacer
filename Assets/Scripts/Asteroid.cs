using UnityEngine;
using System.Collections;

public class Asteroid : Projectile {

    public Vector3 RotationSpeed;
    public bool RotateRandomly = true;

    public override void Start()
    {
        base.Start();
        RotationSpeed = new Vector3(Random.Range(-20, 20), Random.Range(-20, 20), Random.Range(-20, 20));
    }
	
	// Update is called once per frame
	public override void Update () {
        base.Update();
        if (Network.isClient && enabled)
            enabled = false;
        if (RotateRandomly)
            transform.Rotate(RotationSpeed * Time.deltaTime);

	}

    public override void OnTriggerEnter(Collider other)
    {
        // override to not do anything
    }
}
