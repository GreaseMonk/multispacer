using UnityEngine;
using System.Collections;

public class AsteroidLauncher : MonoBehaviour {

    private int currentSalvo = 0;
    public float Interval = 5.0f;

    void Start()
    {
        if (!Network.isServer)
            enabled = false;

        SetNextLaunch((int)Random.Range(1, 8), Interval);
    }

    void SetNextLaunchMP()
    {
        SetNextLaunch((int)Random.Range(1, 8), Interval);
    }

    void SetNextLaunch(int Salvos, float LaunchTime)
    {
        int salvos = Salvos;
        currentSalvo = salvos;
        for (int i = 0; i < salvos; i++)
        {
            Invoke("Launch", LaunchTime);
        }
    }

    void Launch()
    {
        if (Network.peerType != NetworkPeerType.Client && Network.peerType != NetworkPeerType.Server)
            return;

        float RandPosition1 = Random.Range(0, 10.0f);
        float RandPosition2 = Random.Range(0, 10.0f);
        Vector3 randomPosition = new Vector3(transform.position.x + RandPosition1, 0, transform.position.z + RandPosition2);
        GameObject proj = Network.Instantiate(Resources.Load("Prefabs/Asteroid"), randomPosition, Quaternion.identity, 0) as GameObject;
        proj.transform.forward = transform.forward;
        proj.GetComponent<Asteroid>().speed = 5.0f;
        proj.GetComponent<Asteroid>().direction = transform.forward;
        currentSalvo--;

        if (currentSalvo == 0)
            Invoke("SetNextLaunchMP", Interval);
    }
}
