using UnityEngine;

[RequireComponent(typeof(playerController))]
[RequireComponent(typeof(Rigidbody))]
public class InputManager : MonoBehaviour 
{
    public const float speed = 10.0f;
    private Vector3 left    { get { return (new Vector3(-1, 0, 0)   * speed) * Time.deltaTime; } }
    private Vector3 right   { get { return (new Vector3(1, 0, 0)    * speed) * Time.deltaTime; } }
    private Vector3 up      { get { return (new Vector3(0, 0, 1)    * speed) * Time.deltaTime; } }
    private Vector3 down    { get { return (new Vector3(0, 0, -1)   * speed) * Time.deltaTime; } }
    public Vector3 lastDirection;
    private playerController player;
    //private int playerNumber = System.Convert.ToInt32(Network.player.ToString());

    void Start()
    {
        player = gameObject.GetComponent<playerController>();
    }

    void Update()
    {
        if (Network.isServer)
            return;

        if (Input.GetKey(KeyCode.A))
            move(left);
        if (Input.GetKey(KeyCode.W))
            move(up);
        if (Input.GetKey(KeyCode.D))
            move(right);
        if (Input.GetKey(KeyCode.S))
            move(down);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            player.networkView.RPC("Shoot", RPCMode.Server, player.transform.position, player.transform.forward);
            player.sound.Play("shoot2");
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            Network.Disconnect();
            Application.Quit();
        }
    }

    void move(Vector3 direction)
    {
        if(Network.peerType == NetworkPeerType.Client)
            networkView.RPC("SendMovement", RPCMode.Server, player.PlayerNumber, direction);
        transform.position += direction;
        setFacing(direction);
    }
    void setFacing(Vector3 rotation)
    {
        transform.forward = rotation;
        if (lastDirection != rotation)
            lastDirection = rotation;
    }

    [RPC]
    public void SendMovement(int PlayerNumber, Vector3 direction)
    {
        Transform trans = null;
        foreach( GameObject go in GameObject.FindGameObjectsWithTag("Player"))
            if(go.name == "Player" + PlayerNumber.ToString())
                trans = go.transform;
        if (trans != null)
            move(direction);
    }
}
