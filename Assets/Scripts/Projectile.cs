using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public float speed = 20.0f;
    public Vector3 direction;
    public GameObject owner;

    public virtual void Start()
    {
        if (Network.isClient)
            enabled = false;
    }

    public virtual void Update()
    {
        if (Network.isClient && enabled)
            enabled = false;
        if ((transform.position.x > 100 || transform.position.z > 100 ||
            transform.position.x < -100 || transform.position.z < -100))
        {
            Network.RemoveRPCs(networkView.viewID);
            Network.Destroy(gameObject);
        }
        else
            transform.position += (direction * speed) * Time.deltaTime;
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        if (other.gameObject != owner && Network.isServer)
        {
            networkView.RPC("HitSound", RPCMode.OthersBuffered);
            Network.RemoveRPCs(other.gameObject.networkView.viewID);
            Network.Destroy(other.gameObject);
            Network.RemoveRPCs(networkView.viewID);
            Network.Destroy(gameObject);
        }
    }

    [RPC]
    public virtual void HitSound()
    {
        SoundManager sound = null;
        foreach (Camera cam in FindObjectsOfType(typeof(Camera)))
            if(cam.gameObject.name == "Main Camera")
                sound = cam.gameObject.GetComponent<SoundManager>();
        sound.Play("ProjectileHit");
    }

    [RPC]
    public void SetOwner(GameObject NewOwner)
    {
        owner = NewOwner;
    }



    dynamic GetGameMaximumScore()
    {
        return default(dynamic);
    }


}