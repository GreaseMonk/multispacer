using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private AudioSource source;
    public Object[] audioClips;

    void Start()
    {
        source = gameObject.GetComponent<AudioSource>();
        audioClips = Resources.LoadAll("Sounds", typeof(AudioClip));
    }
    public AudioClip Get(string audioClipName)
    {
        AudioClip result = null;
        foreach (AudioClip ac in audioClips)
        {
            if (ac != null && ac.name == audioClipName)
            {
                result = ac;
                return result;
            }
        }
        return result;
    }
    public void Play(string audioClip, float volume = 1.0f)
    {
        if (Network.isClient)
        {
            AudioClip playSound = Get(audioClip);
            if (playSound != null)
                source.PlayOneShot(playSound, volume);
            else
                Debug.Log("Couldn't find audioclip: " + audioClip);
        }
    }
}