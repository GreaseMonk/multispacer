using UnityEngine;

[RequireComponent(typeof(NetworkView))]
public class mainGUI : MonoBehaviour {
    public bool internetAvailable = false;
    public bool masterServerAvailable = false;
    public const int maxConnections = 32;
    private float halfScreenWidth = Screen.width / 2;
    private float halfScreenHeight = Screen.height / 2;
    private string InputIP = "";
    public string[] SupportedMPLevels = {"Level1"};

    void Awake()
    {
        DontDestroyOnLoad(this);
        networkView.group = 0;
    }

    void OnGUI()
    {
        if (Network.peerType == NetworkPeerType.Disconnected)
        {
            if (InputIP == "")
                InputIP = Game.NetTools.ExternalIP;

            string connectIP = GUI.TextField(new Rect(halfScreenWidth - 50, halfScreenHeight - 75, 100, 30), InputIP);

            if (connectIP != Game.NetTools.ExternalIP)
                InputIP = connectIP;


            if (GUI.Button(new Rect(halfScreenWidth - 50, halfScreenHeight - 35, 100, 30), "Connect"))
                Network.Connect(connectIP, 8888, "");
            if (GUI.Button(new Rect(halfScreenWidth - 50, halfScreenHeight, 100, 30), "Start Server"))
            {
                Application.LoadLevel(1);
                Network.InitializeServer(maxConnections, 8888, false);
            }

            int p = Network.connections.Length;

            if (Network.isServer)
            {

                GUI.Label(new Rect(10, 20, 100, 30), "Players: " + p.ToString());
            }
            else
            {
                GUI.Label(new Rect(10, 10, 100, 30), "Player " + Network.player.ToString());
                GUI.Label(new Rect(10, 20, 100, 30), "Players: " + p.ToString());
            }
        }
    }

    /*
    private string formNick = ""; //this is the field where the player will put the name to login

    private string formPassword = ""; //this is his password

    string formText = ""; //this field is where the messages sent by PHP script will be in

    // sha512 - LastOneStanding109238012983

    string URL = "http://www.epicserver.nl/sp/request.php"; //change for your URL

    string hash = "cb794a5e9bfea033afdfaae6acca298fbfe372addedb28b7abbdc13fee41d9302ac041283e4080b4d722ece7fdda120de9519ad3eac0468fcebbc86b89fd5d39"; //change your secret code, and remember to change into the PHP file too



    private Rect textrect = new Rect(10, 150, 500, 500); //just make a GUI object rectangle



    void OnGUI()
    {

        GUI.Label(new Rect(10, 10, 80, 20), "Your nick:"); //text with your nick

        GUI.Label(new Rect(10, 30, 80, 20), "Your pass:");



        formNick = GUI.TextField(new Rect(90, 10, 100, 20), formNick); //here you will insert the new value to variable formNick

        formPassword = GUI.TextField(new Rect(90, 30, 100, 20), formPassword); //same as above, but for password



        if (GUI.Button(new Rect(10, 60, 100, 20), "Try login"))
        { //just a button

            Login();

        }

        GUI.TextArea(textrect, formText);

    }



    void Login()
    {

        WWWForm form = new WWWForm(); //here you create a new form connection

        form.AddField("myform_hash", hash); //add your hash code to the field myform_hash, check that this variable name is the same as in PHP file

        form.AddField("myform_nick", formNick);

        form.AddField("myform_pass", formPassword);

        WWW w = new WWW(URL, form); //here we create a var called 'w' and we sync with our URL and the form

        Game.Tools.WaitOneFrame(); //we wait for the form to check the PHP file, so our game dont just hang

        if (w.error != null)
        {

            print(w.error); //if there is an error, tell us

        }
        else
        {

            print("Test ok");

            formText = w.text; //here we return the data our PHP told us

            w.Dispose(); //clear our form in game

        }



        formNick = ""; //just clean our variables

        formPassword = "";

    }*/
}