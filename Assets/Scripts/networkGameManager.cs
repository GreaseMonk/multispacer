using UnityEngine;
using System.Collections;

public class SpawnManager : object
{
    public GameObject Player(NetworkPlayer player)
    {
        int playerNumber = System.Convert.ToInt32(player.ToString());
        GameObject newPlayer = Network.Instantiate(Resources.Load("prefabs/player"), Vector3.zero, Quaternion.identity, playerNumber) as GameObject;
        newPlayer.networkView.RPC("SetPlayerVariable", RPCMode.AllBuffered, player, Network.connections.Length);
        newPlayer.name = "Player" + Network.connections.Length.ToString();
        newPlayer.GetComponent<playerController>().player = player;
        return newPlayer;
    }
    public void Asteroid()
    {
        // TO BE ADDED
        /*GameObject roid = Network.Instantiate(Resources.Load("Prefabs/Asteroid")) as GameObject;
        roid.transform.position = position;
        roid.transform.rotation = rot;
        roid.GetComponent<Asteroid>().dir = direction;
        roid.GetComponent<Asteroid>().RotateRandomly = RotateRandomly;
        roid.GetComponent<Asteroid>().RotationSpeed = RotationSpeed;*/
    }
    public void AsteroidLauncher()
    {
        // TO BE ADDED
        /*GameObject launcher = Network.Instantiate(Resources.Load("Prefabs/AsteroidSpawner")) as GameObject;
        launcher.transform.position = position;
        launcher.transform.rotation = rot;*/
    }
}

[RequireComponent(typeof(NetworkView))]
public class networkGameManager : MonoBehaviour 
{
    public SpawnManager spawn;
    public ArrayList PlayerComponentRefs = new ArrayList();
    public string disconnectedLevel = "Menu";
    
    private int LastLevelPrefix = 0;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }
    void Start()
    {
        spawn = new SpawnManager();
    }

    /************************************************************************/
    /* Default function definitions                                         */
    /************************************************************************/
    void OnPlayerConnected(NetworkPlayer player)
    {
        PlayerComponentRefs.Add(spawn.Player(player));
    }
    void OnServerInitialized()
    {
        Debug.Log(msg.Get(msg.server.initialized));
    }
    void OnConnectedToServer()
    {
        Debug.Log(msg.Get(msg.client.connected));
        //Application.LoadLevel(1);
    }
    void OnPlayerDisconnected(NetworkPlayer player)
    {
        foreach (playerController p in FindObjectsOfType(typeof(playerController)))
            if (p.player == player)
            {
                Network.RemoveRPCs(p.gameObject.GetComponent<NetworkView>().viewID);
                Network.Destroy(p.gameObject);
            }
        Network.RemoveRPCs(player);
        Network.DestroyPlayerObjects(player);
    }
    void OnDisconnectedFromServer(NetworkDisconnection info)
    {
        if (Network.isServer)
            Debug.Log(msg.Get(msg.server.disconnected));
        else
        {
            if (info == NetworkDisconnection.LostConnection)
                Debug.Log(msg.Get(msg.client.lostconnection));
            else
                Debug.Log(msg.Get(msg.client.disconnected));

            Application.LoadLevel(disconnectedLevel);
        }

        Destroy(gameObject);
    }
    void OnFailedToConnect(NetworkConnectionError error)
    {
        Debug.Log(msg.Get(msg.client.failedconnect) + error);
    }
    void OnNetworkInstantiate(NetworkMessageInfo info)
    {
        Debug.Log(msg.Get(msg.client.newinstance) + info.sender);
    }

    /************************************************************************/
    /* Custom function definitions                                          */
    /************************************************************************/

    [RPC]
    void LoadLevel (NetworkPlayer player, string level, int levelPrefix)
    {
        LastLevelPrefix = levelPrefix;

		// There is no reason to send any more data over the network on the default channel,
		// because we are about to load the level, thus all those objects will get deleted anyway
		Network.SetSendingEnabled(0, false);	

		// We need to stop receiving because first the level must be loaded first.
		// Once the level is loaded, rpc's and other state update attached to objects in the level are allowed to fire
		Network.isMessageQueueRunning = false;

		// All network views loaded from a level will get a prefix into their NetworkViewID.
		// This will prevent old updates from clients leaking into a newly created scene.
		Network.SetLevelPrefix(levelPrefix);
		Application.LoadLevel(level);
		Game.Tools.WaitOneFrame();
		Game.Tools.WaitOneFrame();

        // Allow receiving data again
		Network.isMessageQueueRunning = true;
		// Now the level has been loaded and we can start sending out data to clients
		Network.SetSendingEnabled(0, true);


		foreach (GameObject go in FindObjectsOfType(typeof(GameObject)))
			go.SendMessage("OnNetworkLoadedLevel", SendMessageOptions.DontRequireReceiver);	
    }
}