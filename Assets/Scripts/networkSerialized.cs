using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class networkSerialized : MonoBehaviour {

    /************************************************************************/
    /* All moving objects move with XZ
     * This script will synchronize the current transform X and Z and facing.
     */
    /************************************************************************/

    private Transform trans;

    private Vector3 prev1; // last position
    private Vector3 prev2; // older than prev1
    private Vector3 speed;
    public bool Prediction = false;

    void Start()
    {
        if(transform != null)
            trans = transform;

        Network.sendRate = 50;
    }

    void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
        if (trans == null)
            return;

        float X = 0;
        float Z = 0;
        Vector3 fwd = Vector3.zero;
        if (stream.isWriting)
        {
            // Send
            X = Mathf.Round(trans.position.x * 100f) / 100f;
            Z = Mathf.Round(trans.position.z * 100f) / 100f;
            fwd = Vector3.Normalize(trans.forward);

            stream.Serialize(ref X);
            stream.Serialize(ref Z);
            stream.Serialize(ref fwd);
        }
        else
        {
            // Receive
            stream.Serialize(ref X);
            stream.Serialize(ref Z);
            stream.Serialize(ref fwd);

            trans.position = new Vector3(X, 0, Z);
            trans.forward = fwd;

            if (Prediction)
            {
                //prev2 = prev1;
                prev1 = new Vector3(X, 0, Z) - trans.position;
            }
        }
    }

    Vector3 CalculateSpeed()
    {
        Vector3 diff = trans.position - prev1;
        diff /= 20;
        return diff;
    }

    void Update()
    {
        if (!Prediction)
            return;
        Vector3 diff = CalculateSpeed();
        if (diff != Vector3.zero)
            trans.position -= diff * Time.deltaTime;
    }
}
