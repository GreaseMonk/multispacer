using UnityEngine;

[RequireComponent(typeof(InputManager))]
public class playerController : MonoBehaviour 
{
    public InputManager input;
    public networkSerialized net;
    public NetworkPlayer player;
    public GameObject playerCamera;
    public SoundManager sound;
    public int PlayerNumber = 0;

    void Start()
    {
        if (Network.peerType == NetworkPeerType.Server || player != Network.player)
        {
            enabled = false;
            input.enabled = false;
        }

        foreach (GameObject cam in FindObjectsOfType(typeof(GameObject)))
            if (cam.tag == "MainCamera")
                playerCamera = cam;
        if (playerCamera != null && playerCamera.GetComponent<SoundManager>() != null)
            sound = playerCamera.GetComponent<SoundManager>();
        else
            Debug.LogWarning(msg.Get(msg.sound.missing));
    }
    
    [RPC]
    void Shoot(Vector3 pos, Vector3 dir)
    {
        GameObject proj = Network.Instantiate(Resources.Load("Prefabs/Projectile"), pos, Quaternion.identity, 0) as GameObject;
        proj.transform.forward = dir;
        proj.GetComponent<Projectile>().direction = dir;
        proj.GetComponent<Projectile>().SetOwner(gameObject);
    }

    [RPC]
    void SetPlayerVariable(NetworkPlayer actualplayer, int number)
    {
        if (actualplayer == Network.player)
        {
            player = actualplayer;
            PlayerNumber = number;
        }
    }
}
