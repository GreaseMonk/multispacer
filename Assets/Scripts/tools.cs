using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Linq;
using System.Xml;
using System.IO;

public static class msg
{
    public struct sound
    {
        private const string prefix = "/sound/";
        public const string missing = prefix + "missing";
    }
    public struct server
    {
        private const string prefix = "/server/";
        public const string initialized = prefix + "initialized";
        public const string disconnected = prefix + "disconnected";
        public const string playerdisconnected = prefix + "playerdisconnected";
    }
    public struct client
    {
        private const string prefix = "/client/";
        public const string connected = prefix + "connected";
        public const string disconnected = prefix + "disconnected";
        public const string lostconnection = prefix + "lostconnection";
        public const string failedconnect = prefix + "failedconnect";
        public const string newinstance = prefix + "newinstance";
    }
    public struct hud
    {
        private const string prefix = "/hud/";
    }

    public static string Get(string query)
    {
        /*XmlDocument doc = new XmlDocument();
        doc.Load("Assets/Resources/Localization/English.xml");
        return doc.SelectSingleNode(query).InnerText;*/
        return query;
    }
}

namespace Game
{
    public static class Properties
    {
        public struct Score
        {
            public int max { get { return 2500; } }
        }
    }

    public static class Tools : object
    {
        public static IEnumerator WaitOneFrame()
        {
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
    public static class NetTools
    {
        public const string localhost = "127.0.0.1";
        public static string ExternalIP
        {
            get { return Dns.GetHostAddresses(Dns.GetHostName())[0].ToString(); }
        }
        public static string InternalIP
        {
            get { return Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork).ToString(); }
        }
        public static bool DomainReachable(string url = "www.google.com")
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Timeout = 2000;
                request.Proxy = null;
                request.Credentials = CredentialCache.DefaultNetworkCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                    return true;
            }
            catch (System.Exception ex)
            {
                Debug.Log(ex);
            }
            return false;
        }
    }
}